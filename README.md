# Veldvenne.LAN stack in a single docker-compose

## References / Credits

- https://github.com/lancachenet/lancache-dns
- https://github.com/lancachenet/monolithic
- https://github.com/lancachenet/logstash
- https://github.com/zeropingheroes/lancache-elk
- https://github.com/zeropingheroes/lancache-filebeat


## Veldvenne.LAN cache server build log

### VM Preparations
- Ubuntu 18.04
- Install without LVM on 12 GB disk
- visudo, allow sudo without password: `%sudo	ALL=(ALL:ALL) NOPASSWD:ALL`
- Enable OpenSSH server
- Attach disk
- Passtrough to VM: `qm set 100 -scsi1 /dev/disk/by-id/XXXXXXXX`
- `fdisk /dev/sdb` then create partition
- `pvcreate /dev/sdb1`
- `vgcreate cache /dev/sdb1`
- `lvcreate -l 100%FREE -n cache cache`
- `mkfs.xfs /dev/cache/cache`
- `mkdir /lancache && mkdir /lancache/{data,logs}`
- Add to /etc/fstab: `/dev/cache/cache /lancache xfs defaults 0 0`
- `cd ansible` in this repo
- `ansible-playbook -i hosts playbook.yml` to install docker & docker-compose (or do it yourself)
- `systemctl disable systemd-resolved`
- `systemctl stop systemd-resolved`
- `echo nameserver 1.1.1.1 > /etc/resolv.conf`
- Prevent docker from starting before lancache drive mount
  `systemctl edit --full docker.service` then append `lancache.mount` to After=
- Install netdata: `bash <(curl -Ss https://my-netdata.io/kickstart.sh)`

### Installing lancache
- `git clone https://gitlab.com/veldvenne.lan/cache && cd cache`
- `docker-compose up -d`
- Go to http://10.0.0.2:5601 -> Management -> Saved Objects -> Import dashboards.json
- `dig @10.0.0.2 content1.steampowered.com` should return 10.0.0.2
- see shiny things on http://10.0.0.2:19999
- point all dns resolvers to 10.0.0.2

### Adding more disks

In proxmox:
```
# attach disk to vm 100 (cache) on scsi2
qm set 100 -scsi2 /dev/disk/by-id/XXXXXXXXXXXXXXXX
```
In cache vm
```
fdisk /dev/sdc # create new partition
pvcreate /dev/sdc1
vgextend cache /dev/sdc1
lvextend -l +100%FREE /dev/cache/cache
xfs_growfs /dev/cache/cache
```
